# Challenge GD4H - Métamorph'OSE

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Website</a> 

## Métamorph'OSE

Understanding the healthcare challenges facing a given region requires access to a wealth of data, expertise and ways of visualizing them. The Observatoire régional de santé (ORS) Auvergne-Rhône-Alpes has set up an OSE (Observation en Santé Environnement) database on its BALISES platform, which enables users to view a range of environmental health indicators at different geographical scales in spreadsheet format.

<a href="https://gd4h.ecologie.gouv.fr/defis/793" target="_blank" rel="noreferrer">Find out more about the challenge</a>

## Program installation

```bash
cd metamorphose/
# Possibly create a virtual environment before
# see [here](https://docs.python.org/3/library/venv.html)
pip install -r requirements.txt
```
## Execution

```python
python3 app.py
```
The program will run a web server, and display the url to be consulted in your browser. It will look like this:

```
~/work/GD4H_MetamorphOSE/metamorphose $ python3 app.py
Dash is running on http://127.0.0.1:8050/

 * Serving Flask app 'app'
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment.
Use a production WSGI server instead.
 * Running on http://127.0.0.1:8050
```

## Docker

Warning: the build takes an hour (literally) (pandas library is to blame).
At first glance, you don't need it to work on your machines.

```bash
docker build . -t metamorphose && docker run -it --rm -p 5007:5000 metamorphose
```

## Data preparation

### `DATA_v2.xlsx`

To update the data:
- - re-encode the base file in csv. The aim of working in csv is to avoid having binary files on git. files on git. That's why:
    - change column names `*_starnardisés` to `_standard`
    - export the file as csv, with the following options:
        - separator: `;`
        - encoding: `ISO-8859-3`
- OR modify `src/dataset.py` to make it directly read the .xlsx,
view issue #27.

The notebook must then be re-run `02_stats_indicateurs.ipynb` which will upstream histograms (see below).

## Code architecture

### Overview

The code is divided into two subsets:
- a number of notebooks used during development,
- the program itself.

The program doesn't need the notebooks to run, but (as it stands) these notebooks will have to be re-executed to update the OSE dataset or the geographic data (polygons and EPCI composition). See below..

### The executable program

This code is based on the *plotly* and *dash* libraries:
- *dash* is used to generate *frontend* code in python (implicitly, no html or javascript is required in this project)
- plotly is used to create interactive graphs.

The code is organized in a relatively modular way:
- `app.py` defines the page architecture and the various *callbacks*.
(the functions that link the various interface elements together, for example
for example, to update the map when a menu is selected),
- `src/` package contains a number of utilities designed to be as data-independent as possible. as data-independent as possible (this objective is not perfectly respected),
- `src/definitions.py` is where all our dataset-specific parameters (indicator names, descriptions, etc.) are stored,
- `src/dataset.py` manages the dataset (reading and some pre-processing),
- `src/shapefiles.py`stores and provides access to the different shapefiles,
- `src/maillage.py` manages the tree structure of different meshes (for example, the fact that a given commune belongs to a given EPCI),
- `src/carto.py` provides a function for creating maps,
- `src/portrait.py` builds a portrait of a given territory.

### Notebooks

Notebooks were built along the way to explore and condition the data. A programming paradigm was to calculate things upstream as much as possible, which was done thanks to these notebooks. They are divided into four groups, identified by a number:
- those starting with **00_** are used to convert shapefiles (communes, epci, département) into geojson (whose properties are sorted to retain only those and, above all, whose contours are simplified to make the file more the file more suitable for a website),
- **01_maillage_territorial.ipynb** extracts the list of communes by EPCI,
- **02_stats_indicateurs.ipynb** calculates histograms for each indicator at commune and EPCI levels.

As the program stands, only this last notebook should be re-executed to update the data. The others may also need to be need to be re-executed, but less likely (if communes decide to change change EPCIs, for example, or if the Drôme invades the Ardèche).

### **Contributing**

If you wish to conribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

## Deployment

The current deployment solution is temporary. There is no CI/CD in place. However, notes are available [here](DEPLOIEMENT.md)

### **Licence**

The code is published under [MIT Licence](/LICENSE).

The data referenced in this README and in the installation guide is published under <a href="https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf">Open Licence 2.0</a>.
