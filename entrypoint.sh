#!/bin/sh

gunicorn --bind 0.0.0.0:5000 app:server \
    --worker-class gevent \
    --workers 1 \
    --timeout 1000

