import json
import os
from . import definitions as defs


"""
Ce module charge les geojson qu'on va vouloir utiliser, pour y acceder
facilement (et de manière centralisée)
"""

_shapefiles = {}
for k in defs.shapefiles_config:
    # faudrait try/catch ici, je le fais pas volontairement car on n'a pas
    # défini de gestion des erreurs donc autant que ça pète...
    filename = defs.data_dir + os.path.sep + defs.shapefiles_config[k]["file"]
    with open(filename) as f:
        _shapefiles[k] = json.load(f)


def get(shapefile):
    """
    Renvoie un shapefile étant donné son nom.
    """
    if shapefile in _shapefiles:
        return _shapefiles[shapefile] # un poil lourdingues mes noms de variable
    raise ValueError(f"unexpected shapefile {shapefile}")


def adapt_to(shapefile, indicateur):
    """
    Si le shapefile courant n'est pas disponible pour l'indicateur donné,
    renvoie un indicateur adéquat.
    """
    if shapefile in defs.data_config[indicateur]["mailles"]:
        return shapefile
    return defs.data_config[indicateur]["mailles"][0]


def available_options(indicateur):
    """
    Renvoie la liste des indicateuirs disponibles pour un shapefile donné
    (dans le format qui convient à l'objet "dropdown" de dash).
    """
    out = []
    for d in defs.liste_shapefiles():
        disabled = d not in defs.data_config[indicateur]["mailles"]
        out.append({"label": d, "value": d, "disabled": disabled})
    return out

