import json
import os
from . import definitions as defs


"""
Ce module sert à faire correspondre les différents niveaux d'organisation
territoriale (commune / epci / département). Les calculs ont été fait
préalablement, ce code ne fait que manipuler la structure de données idoine.
"""


_json = defs.data_dir + os.path.sep + defs.maillage_config["file"]
with open(_json) as f:
    _maillage = json.load(f)


def get_type(code, au_singulier=False):
    """
    Renvoie le type de maille (commune, etc) selon le code insee.
    """
    if code in _maillage["communes_par_epci"]:
        return "EPCI"
    if code in _maillage["epci_par_departement"]:
        if au_singulier:
            return "département"
        return "Départements"
    if code in _maillage["maille_parent"]:
        if au_singulier:
            return "commune"
        return "Communes"
    raise ValueError(f"unexpected code {code}")


def get_child(code, maille):
    """
    Renvoie les territoires de niveau inférieur (par exemple les communes
    constituant un EPCI)
    """
    if maille == "Département":
        return _maillage["epci_par_departement"][code]
    if maille == "EPCI":
        return _maillage["communes_par_epci"][code]


def get_ancestors(code):
    """
    Renvoie tous les territoires de niveau supérieur, par exemple pour une
    commune l'EPCI correspondant et le département.
    """
    ancestors = []
    while code in _maillage["maille_parent"]:
        ancestors.append(_maillage["maille_parent"][code])
        code = ancestors[-1]
    return ancestors
