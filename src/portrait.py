from dash import dcc, html
import os
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
import numpy as np
from . import dataset as dat
from . import definitions as defs
from . import maillage
from . import stats_indicateurs


"""
Ce module permet de générer le "portrait" d'un territoire donné, c'est à dire
l'ensemble des objets dash/plotly qui vont le représenter.
"""


def get(code):
    """
    Calcule et renvoie le portrait du territoire donné. Seule fonction publique
    du module.
    """
    out = []
    maille = maillage.get_type(code)
    code_idx = dat.index_territoires[code]
    out.append(_get_portrait_header(code, code_idx, maille))
    for indic_id in defs.data_config:
        nom_indic = defs.data_config[indic_id]["nom"]
        typ = _get_portrait_type(indic_id, maille, code_idx)
        if typ is not None:
            div = _get_indic_div(nom_indic, indic_id, maille, code, code_idx, typ)
            out.append(div)
    return out


def _get_url_insee(code, maille):
    base_url="https://www.insee.fr/fr/statistiques/2011101?geo="
    if maille == "Départements":
        base_url += "DEP-"
    elif maille == "EPCI":
        base_url += "EPCI-"
    elif maille == "Communes":
        base_url += "COM-"
    else:
        raise ValueError("unexpected value {maille}")
    return base_url + code


def _get_link_insee(code, maille):
    url = _get_url_insee(code, maille)
    return dcc.Link("Dossier Insee", href=url, target="_blank")


def _get_presentation_text(nom, maille):
    if maille == "Départements":
        return ("Les graphes ci-dessous décrivent la manière dont le " +
                f"département {nom} se situe par rapport aux autres " +
                "départements de la région AURA.")
    return ("Les histogrammes ci-dessous décrivent la manière dont " +
            f"{nom} se positionne par rapport aux autres " +
            f"{maille.lower()} de la région AURA.")


def _get_portrait_header(code, code_idx, maille):
    """
    Renvoie l'en-tête du portrait de territoire avec un titre et quelques
    métadonnées (lien vers le site de l'insee, etc).
    """
    nom_territoire = dat.df.iloc[code_idx]["nom_territoire"]
    return html.Div([
            html.H2(nom_territoire),
            html.P(f"code insee: {code}"),
            _get_link_insee(code, maille),
            html.P(_get_presentation_text(nom_territoire, maille)),
            html.Hr(),
        ])

def _get_portrait_type(indic_id, maille, code_idx):
    """
    L'affichage est réalisé différemment pour les Départements (barplot
    avec le nom de chaque département) et pour le reste (histogramme)
    """
    if maille not in defs.data_config[indic_id]["mailles"]:
        return None
    if np.isnan(dat.df.iloc[code_idx][indic_id]):
        return None
    if maille == "Départements":
        return "barplot"
    return "histogram"


def _get_indic_div(nom_indic, indic_id, maille, code, code_idx, mode):
    """
    Renvoie la "div" html correspondant à un indicateur, avec:
    - un header (titre, description)
    - le graphique correspondant
    """
    val_territoire = dat.df.iloc[code_idx][indic_id]
    div = _empty_indic_div(nom_indic, indic_id, code, code_idx, val_territoire)
    fig = _get_indic_graph(indic_id, maille, code, mode, val_territoire)
    div.children.append(fig)
    return div


def _empty_indic_div(nom_indic, indic_id, code, code_idx, val_territoire):
    """
    Crée pour un indicateur donné la partie qui complète le graphique (titre,
    bouton pour voir la carte, valeur pour le territoire sélectionné)
    """
    nom_territoire = dat.df.iloc[code_idx]["nom_territoire"]
    if "format" in defs.data_config[indic_id]:
        txt_territoire = defs.data_config[indic_id]["format"] % val_territoire
    else:
        txt_territoire = str(val_territoire)
    btn_id = indic_id + "-btn"
    return html.Div([
            html.Div([
                html.H3(nom_indic, style={"display": "inline",
                                      "padding-right": "50px"}),
                html.Button("Carte",
                    id={"type": "goto-map-btn", "indic": indic_id})
            ], style={"display":"inline-block"}),
            html.P(defs.data_config[indic_id]["description"]),
            html.P(nom_territoire + " : " + txt_territoire)
        ], id=indic_id+'-top-div')


def _get_indic_graph(indic_id, maille, code, mode, val_territoire):
    """
    Crée le graphique représentant un indicateur donné
    """
    if mode == "histogram":
        hist = _indic_histogram(indic_id, maille, val_territoire)
        return dcc.Graph(figure=hist, id=indic_id + "-hist")
    elif mode == "barplot":
        hist = _indic_barplot(indic_id, maille, code)
        return dcc.Graph(figure=hist, id=indic_id + "-bars")
    raise ValueError("unexpected display type")


def _indic_histogram(indic_id, maille, val_territoire):
    """
    Représente l'indicateur donné sous forme d'histogramme (en faisant en
    sorte qu'il n'y ait pas d'espace entre les barres).
    """
    h = stats_indicateurs.hists[indic_id][maille]["h"]
    b = stats_indicateurs.hists[indic_id][maille]["b"]
    bw = (b[1] - b[0])
    widths = np.array([bw]*len(h))
    bar_idxs = b[0] + np.cumsum(widths) - widths
    fig = go.Figure()
    bars = go.Bar(x=bar_idxs, width=widths, offset=0, y=h)
    fig.add_trace(bars)
    fig.add_vline(val_territoire, line_width=5, line_color="#FAA363")

    fig.update_layout(
        yaxis_title = f"proportion des {maille.lower()}, %",
    )

    return fig


def _indic_barplot(indic_id, maille, code_territoire):
    """
    Représente l'indicateur donné sous forme de barplot, c'est à dire
    en nommant chaque barre du nom d'un territoire.
    """
    dat_maille = dat.df[dat.df["niveau"]==maille]
    vals = dat_maille[indic_id].values
    noms = dat_maille["nom_territoire"].values
    colors = ['#FAA363' if c == code_territoire else '#636EFA'
                            for c in dat_maille["code_territoire"]]
    fig = go.Figure()
    bars = go.Bar(x=list(noms), y=list(vals), marker_color=colors)
    fig.add_trace(bars)
    fig.update_xaxes(categoryorder="total ascending")
    return fig


