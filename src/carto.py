import pandas as pd
import plotly.express as px
from . import definitions as defs


"""
Ce module sert à définir les figures de carto plotly (l'affichage à
proprement parler étant réalisé dans le fichier ../app.py).

Références:
- https://plotly.com/python/mapbox-county-choropleth/
- https://plotly.com/python/scattermapbox/
"""



def _copy_viewport(args, layout):
    """
    Extrait le viewport (c'est à dire la position de la carte) pour
    le redonner à plotly comme argument, de manière à ce que la carte
    ne bouge pas quand on la met à jour.
    """
    if layout is None:
        return
    if "mapbox.center" not in layout:
        return
    args["center"] = layout['mapbox.center']
    args["zoom"] = layout['mapbox.zoom']


def _merge_indic_config(args, indic):
    """
    Si l'indicateur à une configuration spécifique dans le module
    definitions.py, cette fonction l'intère aux arguments pour la
    génération de la carte.
    """
    if "map_spec" in defs.data_config[indic]:
        custom_spec = defs.data_config[indic]["map_spec"]
        for k in custom_spec:
            if k in args and type(args[k]) == dict:
                args[k] = {**args[k], **custom_spec[k]}
            else:
                args[k] = custom_spec[k]


def _get_data_range(df, indic_id, maille):
    """
    Cherche les maximumns et minimums des valeurs qu'on va afficher, pour
    mettre à jour le colormap. Rendu nécessaire par le fait qu'on n'a qu'un seul
    dataset pour plusieurs niveaux géographiques (communes, epci etc). Voir #2.
    """
    lo = df[df["niveau"]==maille][indic_id].dropna().min()
    hi = df[df["niveau"]==maille][indic_id].dropna().max()
    return lo, hi


def create(dataset, geojson, indic_id, maille, layout, **kwargs):
    """
    Crée une figure plotly pour l'indicateur et le niveau géographique choisis.
    """
    args = {**defs.map_config}
    args["range_color"] = _get_data_range(dataset, indic_id, maille)
    _merge_indic_config(args, indic_id)
    _copy_viewport(args, layout)
    fig = px.choropleth_mapbox(dataset, geojson, locations='code_territoire',
                           color=indic_id,
                           featureidkey="properties.code_territoire",
                           mapbox_style="carto-positron",
                           **args,
                          )
    fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
    return fig
