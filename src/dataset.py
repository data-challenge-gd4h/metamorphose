import pandas as pd
import os
from . import definitions as defs

"""
Ouverture du dataset et réalisation d'un peu de préprocessing, pour:
- convertir facilement un code insee en nom de territoire
- retrouver facilement l'indice d'un territoire donné dans le dataset
"""


_csv = defs.data_dir + os.path.sep + defs.dataset["file"]
df = pd.read_csv(_csv, **defs.dataset["parser_args"])

noms_territoires = {}
codes_territoires = {}
index_territoires = {}
liste_territoires = []
for k in range(len(df)):
    code = df.iloc[k]["code_territoire"]
    nom = df.iloc[k]["nom_territoire"]
    noms_territoires[code] = nom
    codes_territoires[nom] = code
    index_territoires[code] = k
    liste_territoires.append(nom)
