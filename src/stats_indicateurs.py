import json
import numpy as np
import os
from . import definitions as defs

"""
Plotly est assez lent pour générer les graphes. Pour l'aider un peu les
histogrammes ont été pré-calculés (voir notebooks/stats_indicateurs.ipynb).
Il s'agit ici de les lire pour que les autres modules y aient accès. Ça ne
va probablement rien changer en terme de performance, mais ça met en place
une architecture pour faire d'autres manips du même type éventuellement.
"""


_json = defs.data_dir + os.path.sep + defs.hists_config["file"]
with open(_json) as f:
    hists = json.load(f)

