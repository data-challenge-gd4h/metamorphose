import os

"""
Ce fichier contient:
- tout le paramétrage de l'outil (par exemple: quels indicateurs sont
  accessibles, comment chacun est configuré, etc)
- quelques fonctions 'helpers' pour y accéder.

L'objectif est de ne mettre ici que des définitions, et que le reste du code
soit "data-agnostique" autant que possible.

Ce serait mieux que tout ce paramétrage soit défini dans un fichier (json, ini
ou autre) de manière à ce que le code puisse vraiment être indépendant de ce
qu'il représente.
"""


# Chemin des données défini de manière relative à ce fichier,
# mais qu'on peut surcharger par une variable d'environnement DATA_DIR
# (légèrement overkill)
src_dir = os.path.dirname(__file__)
data_dir = os.path.dirname(src_dir) + os.path.sep + "data"
data_dir = os.getenv("DATA_DIR", data_dir)



# Cette structure retranscrit le fichier excel qui décrit les paramètres.
# L'idée est d'y ajouter pour chaques indicateurs les réglages "custom"
# appropriés.
data_config = {
    "ind_20": {  # Nom de la colonne dans le fichier excel (/csv)
        "description": # qui sera affichée dans l'onglet "portraits"
            "Part de la Surface Agricole Utile en bio",
        "nom": # tel qu'il apparaitra dans la liste des indicateurs
            "Part de la S.A.U. en BIO",
        "mailles": # auxquelles l'indicateur est disponible.
            ["EPCI", "Départements", "Région"],
        "format": "%.2f %%", # comment afficher une valeur, utilisé dans
                             # les portraits de territoire (par exemple
                             # pour celui-ci: "Lyon: 1.22 %"
        "map_spec": { # optionnel: réglages spécifiques de la carte.
                      # ici on mettrait des paramètres de la fonction
                      # px.choropleth_mapbox(). Voir sa doc, carto.py, et un
                      # exemple pour le radon plus bas (ind_13).
        },
    },
    "ind_1": {
        "description": "Exposition moyenne de la population aux PM2,5 " +
            "(en µg/m3)",
        "nom": "Particules Fines: exposition moyenne",
        "format": "%.2f %%",
        "mailles": ["Communes"],
    },
    "ind_3": {
        "description": "Part de la population exposée au dépassement en " +
            "PM2,5 (valeur OMS)",
        "nom": "Particules fines: dépassements",
        "format": "%.2f %%",
        "mailles": ["Communes"],
    },
    "ind_12": {
        "description": "Nombre d’établissements recevant des populations " +
            "vulnérables (ERPV)",
        "nom": "Nombre d'EPRV",
        "format": "%d",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_13": {
        "description": "potentiel d’émission de radon par le sol",
        "nom": "Radon",
        "format": "%d",
        "mailles": ["Communes"],
        "map_spec": {
            "color_continuous_scale": "Portland", # Vert Jaune Rouge irait
                                                  # mieux, c'est ce que j'ai
                                                  # trouvé de plus proche
        },
    },
    "ind_4": {
        "description": "Part de la surface soumise à un niveau de bruit " +
            "inférieur ou égale à 60 dB(A)",
        "nom": "Bruit: part de la surface dont le bruit est modéré",
        "format": "%.2f %%",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_51": {
        "description": "Nombre d’habitants exposés à un niveau de bruit " +
            "supérieur à 70 dB(A)",
        "nom": "Bruit: nombre d’habitants exposés à un bruit gênant",
        "format": "%d",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_52": {
        "description": "Part de la population exposée à un niveau de bruit " +
            "supérieur à 70 dB(A)",
        "nom": "Bruit: proportion des habitants exposés à un bruit gênant",
        "format": "%.2f %%",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_61": {
        "description": "Nombre d’habitants exposés à des niveaux de " +
            "coexposition air-bruit (Orhane 5+6)",
        "nom": "Air et bruit: nombre d’habitants exposés",
        "format": "%d",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_62": {
        "description": "Part de la population exposée à des niveaux de " +
            "coexposition air-bruit (Orhane 5+6)",
        "nom": "Air et bruit: proportion des habitants exposés",
        "format": "%.2f %%",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_17": {
        "description": "Part de la population alimentée par de l’eau " +
            "respectant en permanence les limites de qualité pour les " +
            "nitrates",
        "nom": "Qualité de l’eau : NITRATES",
        "format": "%.2f %%",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_18": {
        "description": "Part de la population alimentée par de l’eau " +
            "respectant en permanence les limites de qualité pour les " +
            "pesticides",
        "nom": "Qualité de l’eau : PESTICIDES",
        "format": "%.2f %%",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_19": {
        "description": "Part de la population alimentée par de l’eau "
            "respectant les limites de qualité bactériologique",
        "nom": "Qualité de l’eau : BACTÉRIOLOGIQUE",
        "format": "%.2f %%",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_21": {
        "description": "Nombre d’anciens sites industriels et activités " +
            "de service (BASIAS)",
        "nom": "Anciens sites industriels",
        "format": "%d",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_22": {
        "description": "Nombre de sites et sols pollués (ou " +
            "potentiellement), appelant une action des pouvoirs publics " +
            "(BASOL)",
        "nom": "Sites et Sols Pollués",
        "format": "%d",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_251": {
        "description": "Nombre annuel de nouveaux cas",
        "nom": "Légionelles: nombre de cas",
        "format": "%d",
        "mailles": ["Départements", "Région"],
    },
    "ind_252": {
        "description": "Taux d’incidence annuel",
        "nom": "Légionelles: taux d'incidence",
        "format": "%.2f %%",
        "mailles": ["Départements", "Région"],
    },
    "ind_15": {
        "description": "Proportion de déplacements domicile/travail " +
            "réalisés en marchant",
        "nom": "Mobilité: marche",
        "format": "%.2f %%",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_16": {
        "description": "Proportion de déplacements domicile/travail " +
            "réalisés en transports en commun",
        "nom": "Mobilité: transports en commun",
        "format": "%.2f %%",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_14": {
        "description": "Proportion de déplacements domicile/travail " +
            "réalisés en véhicules individuels motorisés",
        "nom": "Mobilité: véhicules individuels",
        "format": "%.2f %%",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_23": {
        "description": "Nombre d’épisodes d’intoxication",
        "nom": "Monoxyde de carbone: episodes d'intoxication",
        "format": "%d",
        "mailles": ["Départements", "Région"],
    },
    "ind_24": {
        "description": "Nombre de personnes intoxiquées",
        "nom": "Monoxyde de Carbone: personnes intoxiquées",
        "format": "%d",
        "mailles": ["Départements", "Région"],
    },
    "ind_26": {
        "description": "Nombre de cas incidents de saturnisme infantile",
        "nom": "Plomb: saturnisme infantile",
        "format": "%d",
        "mailles": ["Départements", "Région"],
    },
    "ind_10": {
        "description": "Nombre de jours avec un risque allergique supérieur " +
            "à 3",
        "nom": "Pollens de bouleaux: risque allergique",
        "format": "%d / an",
        "mailles": ["Départements"],
    },
    "ind_11": {
        "description": "Nombre de jours avec un risque allergique aux pollens "+
        "de granimées supérieur à 3",
        "nom": "Pollens de graminées: risque allergique",
        "format": "%d / an",
        "mailles": ["Départements"],
    },
    "ind_8": {
        "description": "Nombre de jours avec un risque allergique supérieur " +
            "à 3",
        "nom": "Pollens d'ambroisie: risque allergique",
        "format": "%d / an",
        "mailles": ["Départements"],
    },
    "ind_91": {
        "description": "Nombre de personnes exposées à un risque allergique " +
            "à l’ambroisie supérieur à 3",
        "nom": "Pollens d'ambroisie: nombre de personnes exposées",
        "format": "%d",
        "mailles": ["Départements", "Région"],
    },
    "ind_92": {
        "description": "Part de la population exposée à un risque allergique " +
            "aux pollens d'ambroisie supérieur à 3",
        "nom": "Pollens d'ambroisie: part de la population exposée",
        "format": "%.2f %%",
        "mailles": ["Départements", "Région"],
    },
    "ind_7_effectif": {
        "description": "Nombre de personnes potentiellement allergique aux " +
            "pollens d'ambroisie",
        "nom": "Pollens d'ambroisie: nombre de personnes potentiellement allergiques",
        "notes": "à voir si ici on choisit l'indicateur 7 (valeur manquantes " +
            "pour les secrets statistiques) ou 79 (fausses valeurs pour les " +
            "secrets statistiques)",
        "format": "%d",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
    "ind_7_taux_standard": {
        "description": "Part de la population potentiellement allergique aux " +
            "pollens d'ambroisie",
        "nom": "Pollens d'ambroisie: proportion de la population potentiellement allergique",
        "format": "%.2f %%",
        "notes": "à voir si ici on choisit l'indicateur 7 (valeur manquantes " +
            "pour les secrets statistiques) ou 79 (fausses valeurs pour les " +
            "secrets statistiques)",
        "mailles": ["Communes", "EPCI", "Départements", "Région"],
    },
}


# Nom de chaque shapefile qu'on voudra pouvoir afficher, et fichier correspondant
shapefiles_config  = {
    "Départements": {
        "file": "depts_aura.geojson",
    },
    "EPCI": {
        "file": "epci_aura.geojson",
    },
    "Communes": {
        "file": "communes_aura.geojson",
    },
}


# nom du fichier définnissant le maillage (e.g. quelle commune va dans quel
# epci)
maillage_config = {
    "file": "maillage.json"
}

# nom du fichier stockank les histogrammes pour chaque indicateur (qui ont été
# calculés en amont, voir ../notebooks/stats_indicateurs.ipynb)
hists_config = {
    "file": "histogrammes_indicateurs.json"
}

# réglage par défaut des vues carto
map_config = {
    "center": { # moyenne centroides (EPCI), ça devrait coller
        "lon": 4.7689,
        "lat": 45.5516,
    },
    "color_continuous_scale": "Viridis",
    "zoom": 7,
    "hover_name": "nom_territoire",
    "hover_data": {
        "code_territoire": False,
    },
}


def liste_indicateurs():
    """
    Renvoie la liste des indicateurs telle que porposée au choix des
    utilisateurs.
    """
    return [{ "label": data_config[k]["nom"], "value": k }
                for k in data_config]


def default_indicateur():
    return liste_indicateurs()[0]


def liste_shapefiles():
    """
    Renvoie la liste de "résolutions administratives" (comment ça s'appelle en
    vrai?) telle que porposée au choix de utilisateurs.
    """
    return list(shapefiles_config.keys())


def default_shapefile():
    return liste_shapefiles()[0]


def _data_converters():
    """
    Tous les types de données ne sont pas correctement devinés par pandas à
    cause de petits bouts de textes insérés ici où là (e.g. "secret stat.").
    La méthode read_csv() de pandas ne gère pas les ValueErrors. Donc on va
    lui dire comment faire.
    On va convertir toute les données en float (même quand ça devrait être
    des entiers) de sorte à pouvoir stocker ces données "invalides" en tant
    que NaN.
    """
    def _parse_float(raw):
        try:
            return float(raw)
        except ValueError:
            return float("nan")
    out = { k: _parse_float for k in data_config }
    return out


# Définition du fichier de données et de la manière de le lire
# (les paramètres de "parser_args" seront passés tels quels
# à la méthode de pandas utilisée dans dataset.py
dataset = {
    "file": "DATA_v2.csv",
    "parser_args": {
        "sep": ";",
        "encoding": "ISO-8859-3",
        "dtype": {"code_territoire": str},
        "converters": _data_converters(),
    }
}
