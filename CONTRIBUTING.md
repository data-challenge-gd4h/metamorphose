# Contributing Guidelines

Thank you for your interest in contributing to our project! We welcome all
contributions, including bug reports, feature requests, documentation
improvements, and code changes.

To ensure that your contributions can be accepted quickly and easily, please
follow these guidelines.


## Reporting Issues

If you find a bug or have a suggestion for a feature, you can open a new
[issue](https://gitlab.com/data-challenge-gd4h/metamorphose/-/issues/new).

When opening an issue:

> 1. Check the [issues](https://gitlab.com/data-challenge-gd4h/metamorphose/-/issues)
and [merge requests](https://gitlab.com/data-challenge-gd4h/metamorphose/-/merge_requests)
to make sure that the feature/bug has not already been addressed, or is being
worked upon.
> 2. Provide as much detail as possible about the issue, including steps to
reproduce the issue if applicable.


## Submitting Merge Requests

We welcome contributions to this repository.

If you want to contribute a feature or fix a bug, please follow these steps:

> 1. Check the [issues](https://gitlab.com/data-challenge-gd4h/metamorphose/-/issues)
and [pull requests](https://gitlab.com/data-challenge-gd4h/metamorphose/-/merge_requests)
to make sure that the feature/bug has not already been addressed, or is being
worked upon.
> 2. Fork the repository and create a new branch for your changes.
> 3. Make the changes in your fork.
> 4. ***Test your changes*** thoroughly to make sure they are working as
expected.
> 5. ***Add documentation*** for your changes, if applicable.
> 6. Open a pull request to this repository.
> 7. In the pull request description, explain the changes you have made and why
they are necessary.


## Coding style


### 1. Formats

Please

- indents = 4 spaces (no tab)
- line length limited to **80** caractères
- fonction lengths limited to ~ 20 lines


### 2. Functions and files scopes

- 1 fonction = 1 fonctionnality, and only one "complexity level". If for
instance we want to open a file and do some processing, better do three
little functions that a bigger:

```python
def load_file(path):   # the "level 1" function, public
    _open_file()       # private
    _process_file()    # private
```

This is a toy example, but you get the point. It might sound silly but it is
not that easy to do correctly.


### 3. Naming

> There are only two hard things in Computer Science: cache invalidation and
naming things. -- Phil Karlton

...please do your best. Semantic rules:

- modules / variables / fonctions names: `snake_case`
- class names: `CamelCase`
- private thing: `_underscore_prefix`

One exception: prefer a single letter for loop indexes, for instance:

`for i in range(...)`

rather than:

`for line_index` in range(...)


### 4. Imports

Never `import *`, always import either a module, or functions explicitely.


### 5. Comments / docstrings

Better a good naming than a useless comment. A useless comment is:

```python
def compute_square_root(a):
   """
   Returns the square root of input   ## USELESS !
   """
   # returns the computed value       ## USELESS !
   return np.sqrt(a)
```


### 6. Commits

Same as for scope, better doing small commits of "atomic" content rather than
big ones.

Last but not least, please format your messages like that:

```
+-------------------------------------------------------+
| subjet: small desctiption                             | max 50 chars
|                                                       | <line jump>
| Some details if necessary,                            | max 80 char after
| possibly on several lines                             |
| cf #27                                                | issue number (if any)
+-------------------------------------------------------+
```


## Code Review

All code changes must be reviewed and approved by at least one member of the
project team before they can be merged into the main branch. Code reviews may
include comments or requests for changes, which should be addressed before the
changes can be merged.

