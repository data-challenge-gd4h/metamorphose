Naviguez vers le bon répertoire :
```bash
cd metamorphose/
```

Créez un environnement virtuel et activez-le:
```bash
python3 -m venv met_env
source met_env/bin/activate
```

Installez les packages requis :
```bash
pip install -r requirements.txt
```

