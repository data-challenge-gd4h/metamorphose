# Déploiement de la solution

## AVERTISSEMENTS IMPORTANTS

La procédure décrite ici accède aux serveurs de la société
[Qonfluens](www.qonfluens.com), sur lesquels vous n'avez pas accès en
écriture. Par conséquent, vous ne **pourrez pas** ré-exécuter ces
instructions telles quelles.

Cette solution d'hébergement a été choisie pour facilliter le travail à la
fin du défi GD4H. Elle est destinée à n'être que temporaire. Selon la
future solution choisie, ces notes seront plus ou moins applicables.

Ce document a deux objectifs:
- constituer une procédure utile tant que la solution est hébergée sur les
serveurs actuels,
- servir de base de travail pour la prochaine version.

Le programme est actuellement déployé au sein d'un cluster *kubernetes*.
C'est une technologie relativement difficile d'accès qui ne sera pas décrite
ici - ces notes s'adressent à quelqu'un du métier.

Les instruction suivantes décrivent ce qu'il faudrait faire pour déployer
le programme autre part *en utilisant le même méthode* (ce qui n'est pas
indispensable, et même peut-être pas souhaitable: si vous n'avez pas de
cluster k8s déjà mis en place ce n'est sans doute pas la façon de faire
la plus simple).


## Instructions

### Prérequis

- avoir accès à un cluster kubernetes,
- avoir un nom de domaine,
- avoir un registry pour les containers (chez docker ou autre)


### Setup de l'url

Ajouter l'adresse du service dans le serveur DNS de l'hébergeur de votre nom
de domaine (si c'est la solution que vous choisissez, il est aussi possible
que ce programme ne soit pas accessible par sa propre url)

- enregistrer un 'A-record' avec l'url choisie, qui pointe vers votre serveur
- ça peut mettre un moment à être mis à jour, tester avec cette commande:
`$ host toto2000.monserveur.com`


### Config k8s

Le fichier .yml doit aussi subir quelques modifications (en gros ça va
consister à remplacer "qonfluens" par ce que vous voulez):
- remplacer l'url actuelle (metamorphose.qonfluens.com) par la votre,
- changer le namespace à votre convenance
- changer le nom de l'image (du registry plutôt) par le votre.

Attention à le relire attentivement svp, il contient plusieurs choses et
je ne voudrais pas vous faire faire des bêtises sur votre cluster... Il y a
en particulier la définition d'un cert-manager (pour https, avec letsencrypt).
Regardez la doc pour ce truc là, je ne suis pas sûr que ça marche from
scratch (où s'il faut d'autres modifications de votre cluster, dans traefik
par exemple)
Le reste est assez standard:
- la définition du namespace
- un ingress
- un déploiement
- un service.


### Build and push du container

- `docker login -u qonfluens`   <= vous ne pourrez pas faire ça, remplacer
"qonfluens" par votre identifiant docker.
- le script `build_and_push_docker.sh` built l'image, la tag avec un tag git
s'il en trouve et la push. Là aussi il faut le modifier pour mettre à jour
la valeur de la variable REGISTRY
Le build du container prend **des plombes**, vous gagnerez du temps à le
`docker pull` avant.


### mise à jour du cluster

Les commandes suivantes permettent ensuite de déployer / mettre à jour (`k`
est un alias pour `kubectl`):

```bash
k config set-context --current --namespace=<your-namespace>
k apply -f k8s.yml
```




