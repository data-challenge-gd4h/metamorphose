#!/bin/bash

# This script builds and pushs the docker image for the current directory,
# with the following extras:
# - check if a git tag exists, and if so:
#    - check that this tag is pushed on git remote
#    - chech that the local repo is clean
#    - if both conditions are Ok, tag the image with that git tag
# Note that if a git tag is found, the docker image is also built and
# pushed without it (so that :latest also refers to that image)

REGISTRY=qonfluens
IM_NAME=metamorphose

# for debug: ser RUN to "echo" for displaying commands instead of
# executing them
RUN=

# If a git tag exists, check the conditions given before
TAG=$(git describe --exact-match --tags HEAD 2>/dev/null)
if [[ ! -z $TAG ]]; then
    echo "found tag $TAG"
    if [[ -z $(git ls-remote origin refs/tags/$TAG) ]]; then
        echo "WARNING, tag $TAG missing in git remote !"
        echo "*** image NOT tagged ***"
        sleep 2
        TAG=
    else
        if [[ ! -z $(git status -s) ]]; then
            echo "WARNING, git status is not clean."
            echo "*** Image NOT tagged ***"
            sleep 2
            TAG=
        fi
    fi
fi

# Build image name(s) and corresponding build command

IMAGE_NAMES="$REGISTRY/$IM_NAME"
if [ ! -z $TAG ]; then
    IMAGE_NAMES="$IMAGE_NAMES $REGISTRY/$IM_NAME:$TAG"
fi

BUILD_CMD="docker build"
for i in $IMAGE_NAMES; do
    BUILD_CMD="$BUILD_CMD -t $i"
done
BUILD_CMD="$BUILD_CMD ."

# Do the actual builds / pushs (or just display commands if $RUN = echo)

set -e

$RUN $BUILD_CMD
for i in $IMAGE_NAMES; do
    $RUN docker push $i
done
