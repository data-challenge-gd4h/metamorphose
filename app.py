from dash import Dash, dcc, html, Input, Output, State, ALL, ctx
import dash_mantine_components as dmc
import src.dataset as dataset
import src.definitions as defs
import src.carto as carto
import src.shapefiles as shapefiles
import src.portrait as portrait
import src.maillage as maillage
import json
import os


"""
Ce fichier définit l'architecture de la page web:
- les différents composants et leur organisation
- les interactions entre eux (explicitées par le décorateur
@app.collabck).
"""


app = Dash(__name__, meta_tags=[{'charset': 'ISO-8859-1'}])
app.title = "Metamorph'OSE"
server = app.server


# Architecture généramle de la page. Les deux Tab permettent de choisir
# d'afficher soit la div '#map-graph', soit '#portrait-main-div'. À noter
# que le contenu de cette dernière est généré par ailleurs (dans
# src/portraits.py)
app.layout = html.Div(
        dmc.LoadingOverlay(dmc.Stack([
            dcc.Tabs(id="tabs-selector", value="map-tab", children=[
                dcc.Tab(label="Cartes des indicateurs", value="map-tab"),
                dcc.Tab(id="portrait-tab", label="Portraits de territoires",
                    value="portrait-tab"),],
                style={"height":"7vh", "font-size": "large",
                    "font-weight": "normal"},
            ),
            html.Div(id="map-div", style={"height":"90vh"}, children=[
                dcc.Dropdown(defs.liste_indicateurs(),
                    defs.default_indicateur(),
                    id='indicator-choice'
                ),
                dcc.Dropdown(defs.liste_shapefiles(),
                    defs.default_shapefile(),
                    id='shapefile-choice'
                ),
                dcc.Graph(id='map-graph', style={"height": "90%"}),
            ]),
            html.Div(id="portrait-div", style={"height":"90vh"},
                className=["portrait-main-div"],
                children=[
                    dcc.Dropdown(dataset.liste_territoires, id="place-choice"),
                    html.Div(id="portrait-custom-div", style={"height":"90vh"},
                        children=[
                            dcc.Markdown("*Choisir un territoire avec le " +
                                "menu ci-dessus (ou en cliquant sur la carte)*")
                    ])
                ]
            ),
            # Les dcc.Store ci dessous servent à stocker et partager des données
            # entre les callbacks. J'en fait plusieurs pour limiter le nombre
            # de callbacks déclenchées à chanque changement.
            dcc.Store(id='store-selected', storage_type='session', data={}),
            dcc.Store(id='store-current', storage_type='session', data={}),
            dcc.Store(id='store-map-view', storage_type='session', data={}),
        ], style={"height":"100vh"}),
    ),
)


@app.callback(
              Output('map-div', 'style'),
              Output('portrait-div', 'style'),
              Input('tabs-selector', 'value'),
              State('map-div', 'style'),
              State('portrait-div', 'style'),
              )
def update_tab(tab, map_style, portrait_style):
    """
    Met à jour ce qui est affiché quand l'utilisateur clique sur un tab.
    """
    def _visible(style):
        return {**style, "display":"block"}
    def _hidden(style):
        return {**style, "display":"none"}
    if tab == "map-tab":
        return _visible(map_style), _hidden(portrait_style)
    return _hidden(map_style), _visible(portrait_style)


@app.callback(
    Output("shapefile-choice", "options"),
    Input("indicator-choice", "value"),
    )
def adapt_shapefile_options(indicateur):
    """
    Met à jour la liste des mailles quand l'utilisateur choisit un indicateur,
    pour indiquer celles qui sont displonibles ou non.
    """
    return shapefiles.available_options(_check_indic(indicateur))


def _check_indic(indic):
    """
    Il semble qu'il y a un bug (ou une mauvaise utilisation) pour obtenir la
    valeur sélectionnée dans le dropdown de choix de l'indicateur. En effet,
    celui ci est paramétré avec une liste de dict {"label":..., "value":...},
    l'idée étant qu'il affiche le label et renvoie la valeur. Mais bizarement
    selon les cas dash donne aux callbacks soit la valeur, soit le dict
    complet... Et fatalement ça plante à un moment donné. Cette fonction
    (assez peu éléguante) sert à contourner ce pb de tuyauterie.
    """
    if type(indic) == str:
        return indic
    return indic["value"]


@app.callback(
    Output("shapefile-choice", "value"),
    Input("indicator-choice", "value"),
    State("shapefile-choice", "value"),
    )
def adapt_shapefile(indicateur, shapefile):
    """
    Met à jour la maille courante si jamais elle n'est pas disponible pour
    l'indicateur choisi.
    """
    return shapefiles.adapt_to(shapefile, _check_indic(indicateur))


@app.callback(
    Output("map-graph", "figure"),
    Output("store-map-view", "data"),
    Input("indicator-choice", "value"),
    Input("shapefile-choice", "value"),
    State('map-graph', 'relayoutData'),
    State('store-map-view', "data"),
    )
def display_choropleth(indic_id, shapefile, layout, store_data):
    """
    Met à jour la carte si l'indicateur ou la maille sont changés. Stocke en
    passant la position courante, de manière à ce que le "viewport" (la position
    de la carte) ne bouge pas.
    """
    geojson = shapefiles.get(shapefile)
    if layout is not None and "mapbox.center" in layout:
        store_data["layout"] = layout
    else:
        if "layout" in store_data:
            layout = store_data["layout"]
    return carto.create(dataset.df, geojson, _check_indic(indic_id), shapefile,
            layout), store_data


@app.callback(
    Output("store-selected", "data", allow_duplicate=True),
    Input("map-graph", "clickData"),
    State("store-selected", "data"),
    prevent_initial_call=True
    )
def store_clicked_place(clickData, store_data):
    """
    Quand l'utilisateur clique sur un polygone, le territoire correspondant
    est stocké dans un "store" (un moyen de stocker des données et de les
    partager entre les callbacks). Le changement de cet objet déclenchera 
    la mise à jour du nom du tab.
    """
    if clickData is None:
        return store_data
    if "points" not in clickData:
        return  store_data
    if len(clickData["points"]) == 0:
        return  store_data
    if "location" not in clickData["points"][0]:
        return  store_data
    code = clickData["points"][0]["location"]
    if code not in dataset.noms_territoires:
        return store_data
    store_data["selected"] = code
    return store_data


@app.callback(
    Output("portrait-tab", "label"),
    Input("store-selected", "data"),
    State("portrait-tab", "label"),
    )
def set_portrait_name(store_data, current_label):
    """
    Met à jour le nom du tab "portrait", à partir d'un changement du store
    (objet qui stocke l'état du programme dans le navigateur)
    """
    if "selected" not in store_data:
        return current_label
    if not store_data["selected"]:
        return current_label
    code = store_data["selected"]
    typ = maillage.get_type(code, au_singulier=True)
    nom = dataset.noms_territoires[code]
    return  f"Détail: {typ} {nom}"


@app.callback(
    Output("portrait-custom-div", "children"),
    Output("store-current", "data"),
    Input('tabs-selector', 'value'),
    State("portrait-custom-div", "children"),
    State("store-selected", "data"),
    State("store-current", "data"),
    )
def display_portrait(selected_tab, current, store_selected, store_current):
    """
    Calcule le portrait de territoire quand l'utilisateur clique sur le tab
    correspondant.
    """
    if selected_tab == "map-tab":
        return current, store_current
    if "selected" not in store_selected:
        return current, store_current
    if "last-portrait-computed" in store_current:
        if store_current["last-portrait-computed"] == store_selected["selected"]:
            return current, store_current
    store_current["last-portrait-computed"] = store_selected["selected"]
    return portrait.get(store_selected["selected"]), store_current


@app.callback(
    Output('tabs-selector', 'value', allow_duplicate=True),
    Output("indicator-choice", "value"),
    Input({"type": "goto-map-btn", "indic": ALL}, "n_clicks"),
    State('tabs-selector', 'value'),
    State("indicator-choice", "value"),
    prevent_initial_call=True
)
def goto_map_btn_handler(clicks, current_tab, current_indic):
    """
    Renvoie à la carte (en adaptant l'indicateur représenté) quand
    l'utilisateur clique sur un des boutons "carte" du portrait de territoire.
    """
    # Fonction moins évidente que les autres car c'est une callback pour pleins
    # de boutons générés dynamiquement (sachant qu'on veut pouvoir identifier
    # lequel a été cliqué). Voir cette doc:
    # https://dash.plotly.com/pattern-matching-callbacks
    prop = ctx.triggered_prop_ids
    if len(prop) != 1:
        return current_tab, current_indic
    prop = prop[list(prop.keys())[0]]
    indic_id = prop["indic"]
    nom_indic = defs.data_config[indic_id]["nom"]
    return "map-tab", {"label": nom_indic, "value": indic_id}


@app.callback(
    Output('place-choice', 'value'),
    Output('tabs-selector', 'value', allow_duplicate=True),
    Output("store-selected", "data", allow_duplicate=True),
    Input('place-choice', 'value'),
    State("store-selected", "data"),
    State('tabs-selector', 'value'),
    prevent_initial_call=True
    )
def update_portrait(place_choice, store_selected, current_tab):
    """
    Re-calcule le portrait de territoire quand l'utilisateur en choisit un
    nouveau dans la liste. Cette mise à jour est déclenchée par l'écriture
    dans le "store-selected" (et effectuée par la callback "display_portrait"
    plus haut).
    """
    if not place_choice:
        return None, current_tab, store_selected
    store_selected["selected"] = dataset.codes_territoires[place_choice]
    return None, "portrait-tab", store_selected


if __name__ == '__main__':
    app.run_server()
