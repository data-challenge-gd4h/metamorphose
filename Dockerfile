FROM python:3.9.13-alpine3.16

WORKDIR /usr/src/app

COPY ./requirements.txt .
RUN apk update
RUN echo "http://dl-8.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN apk --no-cache add gcc gfortran build-base wget freetype-dev libpng-dev openblas-dev
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["/usr/src/app/entrypoint.sh"]
