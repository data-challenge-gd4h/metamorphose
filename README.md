# Métamorph'OSE

*For an english translated version of this file, follow this [link](/README.en.md)*


Ce code a été développé dans le cadre du GD4H (<a
href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">
Green Data for Health</a>), une offre de service incubée au sein de l’ECOLAB,
laboratoire d’innovation pour la transition écologique du Commissariat Général
au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a> 


## Métaporph'OSE: enjeux

Comprendre les enjeux de santé auxquels sont confrontés des territoires requiert
l’accès à de nombreuses données, de l’expertise et des manières de les
visualiser pour les appréhender. L’Observatoire régional de santé (ORS)
Auvergne-Rhône-Alpes met en place sur sa plateforme BALISES, une base de données
OSE (Observation en Santé Environnement) qui propose de visualiser une panoplie
d’indicateurs santé environnement à différentes échelles géographiques sous
forme de tableur.

<a href="https://gd4h.ecologie.gouv.fr/defis/793" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>


## Installation du programme

```bash
cd metamorphose/
# Éventuellement créez un environnement virtuel avant
# voir [ici](https://docs.python.org/3/library/venv.html)
pip install -r requirements.txt
```


## Exécution

```python
python3 app.py
```
Le programme va exécuter un serveur web, et afficher l'url à consulter dans
votre navigateur. Ça ressemblera à ça:

```
~/work/GD4H_MetamorphOSE/metamorphose $ python3 app.py
Dash is running on http://127.0.0.1:8050/

 * Serving Flask app 'app'
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment.
Use a production WSGI server instead.
 * Running on http://127.0.0.1:8050
```


## Docker

Attention, le build prend une heure (littéralement) (c'est la faute à pandas).
A priori vous n'en avez pas besoin pour travailler sur vos machines.

```bash
docker build . -t metamorphose && docker run -it --rm -p 5007:5000 metamorphose
```

## Préparation des données

### `DATA_v2.xlsx`

Pour mettre à jour les données, de deux choses l'une:
- faire comme c'est fait pour l'instant, c'est à dire réencoder le fichier de
base en csv. L'objectif de travailler en csv c'est de ne pas avoir de fichiers
binaires sur git. Pour ça:
    - modifier les noms de colonne `*_starnardisés` par `_standard`
    - exporter le fichier en csv, avec les options suivantes:
        - séparateur: `;`
        - encodage: `ISO-8859-3`
- OU modifier `src/dataset.py` pour lui faire lire directement le fichier .xlsx,
voir le ticket #27.

Il faut ensuite réexécuter le notebook `02_stats_indicateurs.ipynb` qui va
calculer les histogrammes en amont (voir plus bas).



## Architecture du code

### Vue d'ensemble

Le code se répartit en deux sous-ensembles:
- un certain nombre de notebooks ayant accompagné le développement,
- le programme en tant que tel.

Le programme n'a pas besoin des notebooks pour fonctionner, mais (en l'état)
ces notebooks devront être ré-exécutés pour mettre à jour le dataset OSE ou
les données géographiques (polygones et composition des EPCI). Voir plus bas.


### Le programme exécutable

Ce code se base sur les librairies *plotly* et *dash*:
- *dash* permet de générer du code *frontend* en python (de manière implicite,
on n'a pas à manipuler de html ou de javascript dans ce projet)
- plotly sert à créer des graphes interactifs.

Le code est organisé de manière relativement modulaire:
- `app.py` définit l'architecture de la page et les différentes *callbacks*
(les fonctions qui lient entre eux les différents éléments d'interface, par
exemple pour mettre à jour la carte suite au choix d'un menu),
- le package `src/` contient un certain nombre d'utilitaires, pensés pour être
aussi indépendants des données que possible (cet objectif n'est pas parfaitement
respecté),
- `src/definitions.py` est l'endroit où est réuni tout le paramétrage
spécifique à notre dataset (nom des indicateurs, descriptions, etc.
- `src/dataset.py` gère le dataset (lecture et un peu de pré-processing),
- `src/shapefiles.py`stocke et permet d'accéder aus différents shapefiles
- `src/maillage.py` gère l'arborescence des différentes mailles (par exemple
le fait que telle commune appartient à tel EPCI)
- `src/carto.py` fournit une fonction pour créer les cartes,
- `src/portrait.py` construit le portrait d'un territoire donné.


### Les notebooks

Des notebooks ont été construits au fil de l'eau pour explorer et conditionner
les données. Un paradygme de programmation a été de calculer des choses en amont
autant que possible, ce qui a été fait grâce à ces notebooks. Ils sont répartis
en quatre groupes, identifiés par un numéro:
- ceux qui commencent par **00_** servent à convertir des shapefiles (communes,
epci, département) en geojson (dont les propriétés sont triées pour ne conserver
que celles qui servent, et surtout dont les contours sont simplifiés pour rendre
le fichier plus convenable à un site web),
- **01_maillage_territorial.ipynb** extrait la liste des communes par EPCI
- **02_stats_indicateurs.ipynb** calcule des histogrammes pour chaque indicateur
aux niveaux des communes et des EPCI.

En l'état du programme, seul ce dernier notebook devrait être ré-exécuté pour
mettre à jour les données. Les autres pourraient aussi avoir besoin d'être
exécutés à nouveau, mais de manière moins probable (si des communes décidaient
de changer d'EPCI pas exemple, ou si la Drôme envahissait l'Ardèche).


## Contributions

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

## Déploiement

La solution de déploiement actuelle est temporaire. Il n'y a pas (encore!) de
CI/CD mis en place. Des notes sont néanmoins displonibles [ici](DEPLOIEMENT.md)

## Licence

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont
publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).
